import React from 'react'
import { shallow } from 'enzyme'
import DisplayPlayers from './DisplayPlayers'

describe('DisplayPlayers component', () => {
    describe('when provided with array of players', () => {
        it('passes them to the Player components', () => {
            const players = [
                {
                    id: 0,
                    firstname: 'Stan',
                    lastname: "Wawrinka",
                    shortname: "S.WAW",
                    sex: "M",
                    country: {
                        picture: "https://i.eurosport.com/_iss_/geo/country/flag/large/2213.png",
                        code: "SUI"
                    },
                    picture: "https://i.eurosport.com/_iss_/person/pp_clubteam/large/325225.jpg",
                    data: {
                        rank: 21,
                        points: 1784,
                        weight: 81000,
                        height: 183,
                        age: 33,
                        last: [
                            1,
                            1,
                            1,
                            0,
                            1
                        ]
                    }
                },
                {
                    id: 1,
                    firstname: "Rafael",
                    lastname: "Nadal",
                    shortname: "R.NAD",
                    sex: "M",
                    country: {
                        picture: "https://i.eurosport.com/_iss_/geo/country/flag/large/2203.png",
                        code: "ESP"
                    },
                    picture: "https://i.eurosport.com/_iss_/person/pp_clubteam/large/435121.jpg",
                    data: {
                        rank: 1,
                        points: 1982,
                        weight: 85000,
                        height: 185,
                        age: 33,
                        last: [
                            1,
                            0,
                            0,
                            0,
                            1
                        ]
                    }
                }
            ]
            const displayPlayersInstance = shallow(<DisplayPlayers players={players} isLoading={false} />)
            expect(displayPlayersInstance.exists()).toBe(true)
            expect(displayPlayersInstance).toMatchSnapshot()
            displayPlayersInstance.find('Player').forEach(playerInstance => {
                const playerProps = playerInstance.props()
                const matchingPlayer = players.find(player => player.id === playerProps.id)
                expect(playerProps.firstname).toBe(matchingPlayer.firstname)
            })
        })
    })
})
