import React from 'react'
import Player from '../../components/Player/Player'
import CircularProgress from '@material-ui/core/CircularProgress'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import './DisplayPlayers.scss'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        progress: {
            margin: theme.spacing(2)
        }
    })
)

const DisplayPlayers = (props: any) => {
    const classes = useStyles()
    return (
        <div className={'container'}>
            {
                props.isLoading
                    ? <CircularProgress className={classes.progress} />
                    : props.players.map((player: any, index: number) =>
                        <Player
                            key={index}
                            id={index}
                            firstname={player.firstname}
                            lastname={player.lastname}
                            shortname={player.shortname}
                            countryPicture={player.country.picture}
                            picture={player.picture}
                            rank={player.data.rank}
                            points={player.data.points}
                            weight={player.data.weight}
                            height={player.data.height}
                            age={player.data.age}
                            wins={player.data.wins}
                            losses={player.data.losses}
                        />
                    )
            }
        </div>
    )
}

export default DisplayPlayers
