import React, { useState, useEffect } from 'react'
import './App.css'
import axios from 'axios'
import DisplayPlayers from './containers/DisplayPlayers/DisplayPlayers'
import convertTo from './utils/convertTo'

const App: React.FC = () => {
    const [players, setPlayers] = useState<any[]>([])
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        callPlayers()
    }, [])

    const callPlayers = () => {
        setIsLoading(true)
        axios.get('https://eurosportdigital.github.io/eurosport-web-developer-recruitment/headtohead.json')
            .then((res: any) => {
                const players = res.data.players
                players.map((player: any, index: number) => {
                    Object.assign(player, { id: index })
                    player.data.weight = convertTo(player.data.weight)
                    let wins = 0
                    let losses = 0
                    player.data.last.map((el: number) => {
                        if (el) {
                            Object.assign(player.data, { wins: ++wins })
                        } else {
                            Object.assign(player.data, { losses: ++losses })
                        }
                    })
                })
                setIsLoading(false)
                setPlayers(players)
            })
            .catch((err: any) => {
                setIsLoading(false)
                console.log('err', err)
            })
    }

    return <DisplayPlayers players={players} isLoading={isLoading} />
}

export default App
