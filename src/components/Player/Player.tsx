import React from 'react'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import { red } from '@material-ui/core/colors'
import Avatar from '@material-ui/core/Avatar'
import './Player.scss'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        card: {
            width: 300,
            marginRight: 10,
            ['@media screen and (max-width: 599px)']: {
                marginRight: 0,
                marginBottom: 10
            }
        },
        media: {
            height: 0,
            paddingTop: '56.25%' // 16:9
        },
        expand: {
            transform: 'rotate(0deg)',
            marginLeft: 'auto',
            transition: theme.transitions.create('transform', {
                duration: theme.transitions.duration.shortest
            }),
        },
        expandOpen: {
            transform: 'rotate(180deg)'
        },
        avatar: {
            backgroundColor: red[500],
            fontSize: '0.65rem'
        },
        action: {
            margin: '0 0 0 16px',
            display: 'flex',
            alignSelf: 'center'
        },
    }),
);

const Player = (props: any) => {
    const classes = useStyles();
    return (
        <Card className={classes.card} id={`card-${props.id}`}>
            <CardHeader
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>
                        {props.shortname}
                    </Avatar>
                }
                action={<img src={props.countryPicture} alt="country" width={50} />}
                title={`${props.firstname} ${props.lastname}`}
                subheader={`rank: ${props.rank} point: ${props.points}`}
                classes={{ action: classes.action }}
            />
            <div className={'img-text'}>
                <img src={props.picture} alt="celebrity" />
                <div className={'description'}>
                    <p>{`Weight: ${props.weight}`}</p>
                    <p>{`Height: ${props.height}`}</p>
                    <p>{`Age: ${props.age}`}</p>
                    <div className={'wins-losses'}>
                        <p>{`Wins: ${props.wins}`}</p>
                        <p>{`Losses: ${props.losses}`}</p>
                    </div>
                </div>
            </div>
        </Card>
    )
}

export default Player
