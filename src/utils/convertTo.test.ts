import convertTo from './convertTo'

test('convert 81000 g by 1000 equals 81 kg', () => {
    expect(convertTo(81000)).toBe(81)
})
