import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import axios from 'axios'
// import DisplayPlayers from './containers/DisplayPlayers/DisplayPlayers'
// import { shallow } from 'enzyme'

// jest.mock('axios')

it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<App />, div)
    ReactDOM.unmountComponentAtNode(div)
})


// describe('DisplayPlayers component', () => {
//     describe('when rendered', () => {
//         it('should fetch a list of players', () => {
//             const getSpy = jest.spyOn(axios, 'get')
//             const displayPlayersInstance = shallow(<DisplayPlayers />)
//             expect(getSpy).toBeCalled()
//         })
//     })
// })
